// Lace configuration

export const serverOpts: Deno.ListenOptions = {
    hostname: Deno.env.get("LACE_HOST") || "localhost",
    port: parseInt(Deno.env.get("LACE_PORT") || "8080"),
};

export const browseOpts = {
    /** Root of the file browser. May be a relative path. */
    root: "./srv",
};