// Lace startup

import * as http from "https://deno.land/std@0.76.0/http/server.ts";
import * as path from "https://deno.land/std@0.76.0/path/mod.ts";
import * as config from "./config.ts";

/** Returns a function which resolves a path into a real, absolute path,
 * and caches the result on further calls.
*/
export const realPathCache = (relativePath: string) => {
    let cache = "";

    return async() => {
        if(cache.length > 0) {
            return cache;
        }
        cache = await Deno.realPath(relativePath);
        return cache;
    };
};

/** Joins fp to root, returning an error if the resulting path is outside
 * of the root.
 */
export const safeJoin = async (fp: string, root: string) => {
    const joined = path.join(root, fp);

    if(joined.indexOf('\0') !== -1) {
        return { fp: joined, error: true };
    }
    if(joined.indexOf(root) === -1) {
        return { fp: joined, error: true };
    }
    return { fp: joined, error: false };
}

/** Taking in a root path, returns file handling functions. */
export const genFileHandler = (root: string) => {
    const getRoot = realPathCache(root);

    return {
        /** Tries to find the user provided file path safely. */
        findFile: async (userPath: string) => {
            const { fp, error } = await safeJoin(userPath, await getRoot());
            if(error) {
                return { error: "Path doesn't exist", path: "" };
            }
            return { error: "", path: fp };
        }
    }
};

/** Server init */
async function init() {
    console.log(`Listening on ${config.serverOpts.hostname}:${config.serverOpts.port}`);

    const fileHandler = genFileHandler(config.browseOpts.root);
    const s = http.serve(config.serverOpts);
    
    for await(const req of s) {
        const { path, error } = await fileHandler.findFile(req.url);
        if(error != "") {
            req.respond({ body: error, status: 404, });
            continue;
        }
        try {
            const res = await Deno.stat(path);
            console.log(res);

            if(!res.isDirectory) {
                const h = await Deno.open(path);
                req.respond({ body: h, });
                req.headers.set("content-length", res.size.toString());
                req.headers.set("content-type", "text/plain");
                req.done.then(() => {
                    Deno.close(h.rid);
                })
                continue;
            }
        } catch(err) {
            console.error(err);
            req.respond({ body: "Not found", status: 404 });
            continue;
        }
        req.respond({ body: "got a path" })
    }
}

init();