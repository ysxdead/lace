import { safeJoin, realPathCache } from "./index.ts";
import {
    assert
  } from "https://deno.land/std@0.76.0/testing/asserts.ts";

Deno.test("realPathCache", async () => {
    const root = realPathCache("srv");
    assert((await root()).length > root.length);
});

Deno.test("isInRoot", async () => {
  const fn = async(fp: string) => {
    const res = await safeJoin(fp, await Deno.realPath("./srv"));
    return res.error;
  }
  assert(await fn("../up_one.txt"), "Expected ../ to trigger true");
  assert(await fn("../../up_two.txt"), "Expected ../../ to trigger true");
  assert(!await fn("ok.txt"), "Expected dog.txt to trigger false");
  assert(!await fn("/"), "Expected / to trigger false");
  assert(!await fn("/cat/ferret/deep.txt"), "Expected subdirs to trigger false");
});